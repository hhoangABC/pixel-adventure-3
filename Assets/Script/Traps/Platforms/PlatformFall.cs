﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformFall : MonoBehaviour
{
    [SerializeField] protected float fallTime = 2f;
    [SerializeField] protected float fallDistance = 2f;
    [SerializeField] protected float fallSpeed = 3f;
    protected bool isPlayerOnPlatform = false;
    protected bool isFalling = false;
    protected float currentTime = 0f;
    protected Vector3 originalPosition;

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            collision.gameObject.transform.SetParent(transform);
            isPlayerOnPlatform = true;
            currentTime = 0f;
            originalPosition = transform.position;
        }
    }

    protected void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            collision.gameObject.transform.SetParent(null);
            isPlayerOnPlatform = false;
            isFalling = false;
            currentTime = 0f;
        }
    }

    protected void Update()
    {
        if (isPlayerOnPlatform)
        {
            currentTime += Time.deltaTime;

            if (currentTime >= fallTime && !isFalling)
            {
                isFalling = true;
            }
        }

        if (isFalling)
        {
            transform.Translate(Vector3.down * fallSpeed * Time.deltaTime);

            if (transform.position.y < originalPosition.y - fallDistance) //Thay đổi khoảng cách rơi mong muốn
            {
                Destroy(gameObject);
            }
        }
    }
}
