using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour
{
    protected Rigidbody2D rb2D;
    protected Animator anim;

    [SerializeField] protected AudioSource deathSound;

    protected void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Trap"))
        {
            Die();
        }
    }

    protected virtual void Die()
    {
        deathSound.Play();
        rb2D.bodyType = RigidbodyType2D.Static;
        anim.SetTrigger("death");
    }

    protected virtual void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
