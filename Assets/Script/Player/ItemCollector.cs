using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemCollector : MonoBehaviour
{
    protected int coins = 0;

    [SerializeField] protected Text coinText;

    [SerializeField] protected AudioSource coinSound;

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Coin"))
        {
            coinSound.Play();
            Destroy(collision.gameObject);
            coins++;
            coinText.text = "Point: " + coins;
        }
    }
}
