using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    protected Rigidbody2D rb2D;
    protected BoxCollider2D coll2D;
    protected SpriteRenderer sprite;
    protected Animator anim;

    [SerializeField] protected LayerMask jumpableGround;

    protected float dirX = 0f;
    [SerializeField] protected float moveSpeed = 8f;
    [SerializeField] protected float jumpForce = 14f;

    protected enum MovementState { idle, running, jumping, falling }

    [SerializeField] protected AudioSource jumpSound;

    //double jump
    protected int extraJumps = 1;
    protected int currentExtraJumps;

    [Header("Check enemy")]
    [SerializeField] protected Transform enemyCheck;
    [SerializeField] protected float enemyCheckRadius;

    protected void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        coll2D = GetComponent<BoxCollider2D>();
        sprite = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();

        currentExtraJumps = extraJumps;
    }

    protected void Update()
    {
        dirX = Input.GetAxis("Horizontal");
        rb2D.velocity = new Vector2(dirX * moveSpeed, rb2D.velocity.y);

        CheckForEnemy();

        if (IsGrounded())
        {
            currentExtraJumps = extraJumps;
        }

        if (Input.GetButtonDown("Jump"))
        {
            if (IsGrounded() || currentExtraJumps > 0)
            {
                jumpSound.Play();
                Jump();

                if (!IsGrounded())
                {
                    currentExtraJumps--;
                }
            }
        }

        UpdateAnimation();
    }

    protected void CheckForEnemy()
    {
        Collider2D[] hitedColliders = Physics2D.OverlapCircleAll(enemyCheck.position, enemyCheckRadius);

        foreach (var enemy in hitedColliders)
        {
            if (enemy.GetComponent<Enemy>() != null)
            {
                enemy.GetComponent<Enemy>().Damage();
                Jump();
            }
        }
    }

    protected void Jump()
    {
        rb2D.velocity = new Vector2(rb2D.velocity.x, jumpForce);
    }

    protected virtual void UpdateAnimation()
    {
        MovementState state;

        if (dirX > 0f)
        {
            state = MovementState.running;
            sprite.flipX = false;
        }
        else if (dirX < 0f)
        {
            state = MovementState.running;
            sprite.flipX = true;
        }
        else
        {
            state = MovementState.idle;
        }

        if (rb2D.velocity.y > .1f)
        {
            state = MovementState.jumping;
        }
        else if (rb2D.velocity.y < -.1f)
        {
            state = MovementState.falling;
        }

        anim.SetInteger("state", (int)state);
    }

    protected bool IsGrounded()
    {
        return Physics2D.BoxCast(coll2D.bounds.center, coll2D.bounds.size, 0f, Vector2.down, .1f, jumpableGround);
    }

    protected void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(enemyCheck.position, enemyCheckRadius);
    }
}
