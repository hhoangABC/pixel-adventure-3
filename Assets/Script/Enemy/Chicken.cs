using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chicken : Enemy
{
    [Header("Move info")]
    [SerializeField] protected float speed = 2f;
    [SerializeField] protected float idleTime = 2f;
    protected float idleTimeCounter;

    protected override void Start()
    {
        base.Start();
    }

    protected void Update()
    {
        if (idleTimeCounter < 0)
        {
            rb.velocity = new Vector2(speed * facingDirection, rb.velocity.y);
        }
        else
        {
            rb.velocity = new Vector2(0, 0);
        }

        idleTimeCounter -= Time.deltaTime;

        CollisionChecks();

        if (wallDetected || !groundDetected)
        {
            idleTimeCounter = idleTime;
            Flip();
        }

        anim.SetFloat("xVelocity", rb.velocity.x);
    }
}
