using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuGame : MonoBehaviour
{
    [SerializeField] protected limitsFPS limits;

    public virtual void PlayGame()
    {
        //SceneManager.LoadSceneAsync
    }

    public enum limitsFPS
    {
        limit30 = 31,
        limit60 = 61,
        limit120 = 121,
        limit144 = 145,
    }

    protected void Awake()
    {
        Application.targetFrameRate = (int)limits;
    }


}
