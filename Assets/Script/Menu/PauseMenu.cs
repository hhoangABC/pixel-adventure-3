using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] protected GameObject pauseMenu;

    public virtual void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
    }

    public virtual void Home()
    {
        SceneManager.LoadScene("MenuStart");
        Time.timeScale = 1f;
    }

    public virtual void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    public virtual void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1f;
    }
}
