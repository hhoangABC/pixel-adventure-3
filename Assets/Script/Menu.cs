using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public virtual void StartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public virtual void Restart()
    {
        SceneManager.LoadScene("Level 1");
    }

    public virtual void Quit()
    {
        Application.Quit();
    }
}
